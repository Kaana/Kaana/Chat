# Kaana Chat App

This is a project to create a chat app using Nuxt.js and Vue.js.

## Current Progress

- Installed and set up Nuxt.js and Vue.js using the command `npx create-nuxt-app chat`
- Created a basic layout and components for the app
- Implemented real-time communication using Supabase
- Added user authentication and authorization
- Styled the app using Vuetify.js

## Future Plans

- Add more features such as file sharing and video/audio calls
- Improve the UI and user experience
- Add backend functionality using Node.js and Express.js
- Deploy the app to a live server

## How to run the project

1. Clone the repository
2. Run `yarn install` to install the dependencies
3. Run `yarn dev` to start the development server
4. Open `http://localhost:3000` in your browser to see the app

## Structure

- assets/
  - scss/
    - main.scss
- components/
  - ChatRoom.vue
  - ChatMessage.vue
  - ChatInput.vue
  - ConversationList.vue
  - Profile.vue
  - Search.vue
  - Settings.vue
  - MessageDetails.vue
- layouts/
  - default.vue
- pages/
  - index.vue
  - conversations.vue
  - profile.vue
  - search.vue
  - settings.vue
  - message-details.vue
- plugins/
  - socket.io.js
- static/
- store/
  - index.js
  - actions.js
  - mutations.js
  - state.js
- nuxt.config.js
- package.json
- README.md
